#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import tweepy
import json
import re
from datetime import datetime, timedelta, timezone
import time

import os
import locale

import unidecode
import glob
import json
import statistics
import random


#SAMPLE (INVALID) KEYS
twitter_auth_keys = {
    "consumer_key"        : "tcXrZ4bQ1aRWEJqcUejC1q28q",
    "consumer_secret"     : "DbHExN4gunghn005Fd2m1Mv0PiyPm2o7SZuQzU1HMfv6qKK1va",
    "access_token"        : "1143077983298547712-WsB3W6JYu1wMZy8wOFLWotE3GDMBuz",
    "access_token_secret" : "A9HdLSYTKIldih3TemGc0L0wdJHE9hdOjKDGsHOG9EZAI"
}


''' #ImpactActionAdp '''
IMPACT_SYN_KEYWORD = "#impactactionadp"
IMPACT_SYNACK_KEYWORD = "#RappelActionADP"
IMPACT_DATA_KEYWORD = "Votre #RappelActionADP est prêt."

MSG_NO_INSEE = "Inscrivez le code INSEE voulu après le hashtag ImpactActionADP"
CODE_POSTAUX_FPATH="/carto/code_post.json"
''''''''''''''''''''''''

def name_hashtag(nom):
    '''Normalise les noms de localites trop long ou trop compliques'''
    nom = unidecode.unidecode(nom)
    nom = re.compile('[^a-zA-Z0-9]').sub('', nom)

    if len(nom)>15:
        nom = nom[0:12]+"..."

    return nom

def jsonload(fpath):
    '''Lit un fichier json dans le chemin `fpath`'''
    with open(fpath, "r") as f:
        return json.load(f)

def get_data_paths(link_folder, files_folder):
    '''Renvoie une liste de timestamps disponible par ordre chrono'''
    cwd = os.getcwd()
    os.chdir(link_folder)
    files = glob.glob(files_folder)
    files.sort(key=lambda f:int(f.split('.')[0].split('/')[1]))
    os.chdir(cwd)
    return [f.split('.')[0].split('/')[1] for f in files]

def find_correct_ts(froot, datec, getBeforeDate):
    '''Essaye de trouver le TS correspondant a la date datec. Trouve celui juste avant si getBeforeDate, juste apres sinon.
       Retourne None si echec'''
    TS = get_data_paths(froot+"/archive/carte/", "departements/*.json")
    datec_ts = time.mktime(datec.timetuple())
    for tIdx, ts in enumerate(TS):
        if int(ts) < datec_ts:
            continue

        if not getBeforeDate:
            return ts

        if tIdx == 0:
            return None

        return TS[tIdx-1]

    if getBeforeDate:
        return TS[-1]

    return None

def try_match_improve(text, output, data):
    '''Try to improve matching results when len(output)>1 by finding an exact match'''
    if len(output)<=1:
        return output

    text = ' '.join(text.split())
    text = text.split()
    output_names = [data[key]["nom"].lower() for key in output]
    restricted = list(set(text) & set(output_names))
    if len(restricted) == 1:
        for key in data:
            if data[key]["nom"].lower() == restricted[0]:
                return [key]

    return output


def extract_ville(text, communes, depts, code_post_fpath):
    '''Essaie d'extraire du texte une commune cle du json communes ou un dpt cle du json depts.
       Renvoie code,is_code_commune ou code est une liste de code possible
       Renvoie [], None si echec.
    '''
    text = text.lower().split("#impactactionadp")[-1]

    #first case: on a le code insee/departement dans le tweet, c'est facile
    insee_search = re.search('(\d[\dAB]\d\d\d)', text, re.IGNORECASE)
    if insee_search:
        code = insee_search.group(1).upper()
        if code in communes:
            return [code], True

        #essaye de trouver un match avec les codes postaux
        with open(code_post_fpath, "r") as f:
            code_postaux = json.load(f)
            if code in code_postaux and code_postaux[code] in communes:
                return [code_postaux[code]], True

    dep_search = re.search('(\d[\dAB]\d?)', text, re.IGNORECASE)
    if dep_search:
        if dep_search.group(1).upper() in depts:
            return [dep_search.group(1).upper()], False

    output = []
    for commune in communes:
        if communes[commune]["nom"].lower() in text:
            output.append(commune)

    if len(output) > 0:
            return try_match_improve(text, output, communes), True

    output = []
    for dept in depts:
        output = []
        if depts[dept]["nom"].lower() in text:
            output.append(dept)

    if len(output) > 0:
        return try_match_improve(text, output, dept), True

    return [], None

def TEST_EXTRACT():
    TEST_CASES=[['pantin\nplease', ['93055'], True],[' Pantin\nPlease', ['93055'], True], ['#ImpactActionADP 75016\n\nPour déconner 😉😀...', ['75116'], True]]

    mts = "1562834236" #sample ts to have list of dep/communes
    froot="../archive"
    depts = jsonload(froot+"/archive/carte/departements/"+mts+".data_departements.json")
    communes = jsonload(froot+"/archive/carte/communes/"+mts+".data_communes.json")
    for TEST_CASE in TEST_CASES:
        r, v = extract_ville(TEST_CASE[0], communes, depts, froot+CODE_POSTAUX_FPATH)
        if r != TEST_CASE[1] or v != TEST_CASE[2]:
            print("TEST={} failed ! r={}, v={}".format(TEST_CASE, r, v))

def get_all_my_tweets(api, created_at):
    '''Get all my tweets sent after created_at time'''
    since_id = 1
    output = []
    while True:
        for tweet in tweepy.Cursor(api.user_timeline, id="adpripfr", since_id=since_id, tweet_mode='extended').items():
            since_id = max(tweet.id, since_id)
            if created_at > tweet.created_at:
                return output

            output.append(tweet)

    return output

def SendRappelADP(froot, api, my_tweets, SYN, SYNACK):
    ''' Essaye d'envoyer la reponse finale du rappel pour un #ImpactActionAdp '''
    #check we don't already have answered to this tweet
    for DATA in my_tweets:
        if SYNACK.id == DATA.in_reply_to_status_id and IMPACT_DATA_KEYWORD in DATA.full_text:
            return None

    #wait atrleast 5 days + have data...
    ts_today = find_correct_ts(froot, SYN.created_at+timedelta(days=5), False)
    if ts_today is None:
        print("-> #ImpactActionADP tweet '{}' trop tot, skip!".format(SYN.full_text.replace('\r', '\\r').replace('\n', '\\n')))
        return None

    ts_before = find_correct_ts(froot, SYN.created_at, True)
    depts = jsonload(froot+"/archive/carte/departements/"+ts_today+".data_departements.json")
    communes = jsonload(froot+"/archive/carte/communes/"+ts_today+".data_communes.json")
    requested_insee, isCommune = extract_ville(SYN.full_text, communes, depts, froot+CODE_POSTAUX_FPATH)
    if len(requested_insee)==0:
        print("-> #ImpactActionADP tweet '{}' pas de localite trouvee!".format(SYN.full_text.replace('\r', '\\r').replace('\n', '\\n')))
        return None
    if len(requested_insee)>1:
        print("-> #ImpactActionADP tweet '{}' trop de localites trouvees: {} !".format(SYN.full_text.replace('\r', '\\r').replace('\n', '\\n')), requested_insee)
        return None

    data_before = None
    data_today = None
    if isCommune:
        before = jsonload(froot+"/archive/carte/communes/"+ts_before+".data_communes.json")
        data_today = communes[requested_insee[0]]
        data_before = before[requested_insee[0]]
    else:
        before = jsonload(froot+"/archive/carte/departements/"+ts_before+".data_departements.json")
        data_today = communes[requested_insee[0]]
        data_before = before[requested_insee[0]]

    diff_per = 100*(data_today["soutiens"]/data_before["soutiens"]-1) #/data_today["electeurs"]
    return "Bjr @{} ! ".format(SYN.user.screen_name)+IMPACT_DATA_KEYWORD+" {} est passé de {:n} à {:n} soutiens entre le {} et {}, soit un progrès de {:.2f}% !".format(requested_insee[0], data_before["soutiens"], data_today["soutiens"], SYN.created_at.strftime("%d/%m"),  (SYN.created_at+timedelta(days=5)).strftime("%d/%m"), diff_per)

def SendAckRappelADP(froot, api, SYN):
    '''Alerte un utilisateur qu'on a bien compris qu'il veut un #ImpactActionAdp. Retourne le message a renvoyer, None si rien a faire '''
    answer = IMPACT_SYNACK_KEYWORD+" "
    ts = find_correct_ts(froot, SYN.created_at, True)
    if ts is None:
        return "On a pas de date avant {} chez nous ! Essayez avec un autre tweet.".format(SYN.created_at.strftime("%d/%m/%Y"))

    depts = jsonload(froot+"/archive/carte/departements/"+ts+".data_departements.json")
    communes = jsonload(froot+"/archive/carte/communes/"+ts+".data_communes.json")
    requested_insee, isCommune = extract_ville(SYN.full_text, communes, depts, froot+CODE_POSTAUX_FPATH)
    if len(requested_insee)==0:
        return answer+"Apparement aucune ville ni departement dans votre tweet ! "+MSG_NO_INSEE
    if len(requested_insee)>1:
        return answer+"Plusieurs ({:n}) villes/depts correspondent à votre requête... {}".format(len(requested_insee), MSG_NO_INSEE)

    data = None
    if isCommune:
        data = communes[requested_insee[0]]
    else:
        data = depts[requested_insee[0]]

    return answer+"Bjr @{} ! Le {}, {} ({}) a {:n} soutiens ({:.2f}%). On vous rappelera son score dans 5 jours.".format(SYN.user.screen_name, SYN.created_at.strftime("%d/%m/%Y"), name_hashtag(data["nom"]), requested_insee[0], data["soutiens"], 100*data["soutiens"]/data["electeurs"])


def ImpactActionAdpBot(froot, api):
    since_id = 1
    while True:
        for SYN in tweepy.Cursor(api.mentions_timeline, since_id=since_id, tweet_mode='extended').items():
            since_id = max(SYN.id, since_id)

            #check only 5 days old SYNs (erm 6ish)
            if (datetime.now() - SYN.created_at).days > 7: #1 day safety for time shifts
                return

            #try to look for SYN-ACK - if none, answer a SYN-ACK to the SYN
            if (IMPACT_SYN_KEYWORD in SYN.full_text.lower()) and not (IMPACT_SYNACK_KEYWORD in SYN.full_text.lower()):
                my_tweets = get_all_my_tweets(api, SYN.created_at)
                hasFoundSynAck = False
                for SYNACK in my_tweets:
                    if SYN.id == SYNACK.in_reply_to_status_id and IMPACT_SYNACK_KEYWORD in SYNACK.full_text: #we found or SYN-ACK
                        hasFoundSynAck = True
                        answer = SendRappelADP(froot, api, my_tweets, SYN, SYNACK)
                        if answer != None:
                            print("TWEET DATA SENDING:", SYN.full_text.replace('\r', '\\r').replace('\n', '\\n'), "~~~>", answer)
                            api.update_status("@{} {}".format(SYN.user.screen_name, answer), SYNACK.id)
                        break

                if not hasFoundSynAck:
                    answer = SendAckRappelADP(froot, api, SYN)
                    print("TWEET SYNACK SENDING:", SYN.full_text.replace('\r', '\\r').replace('\n', '\\n'), "~~~>", answer)
                    api.update_status("@{} {}".format(SYN.user.screen_name, answer), SYN.id)




def parse_tweets(froot, auth_keys):
    '''Poste un tweet sur le compte qui correspond aux clefs fournies.
        auth_keys doit contenir : "consumer_key" "consumer_secret" "access_token" "access_token_secret"
    '''
    auth = tweepy.OAuthHandler(auth_keys['consumer_key'], auth_keys['consumer_secret'])
    auth.set_access_token(auth_keys['access_token'], auth_keys['access_token_secret'])
    api = tweepy.API(auth)

    print("Running #ImpactActionAdp bot...")
    ImpactActionAdpBot(froot, api)
