#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

from operator import itemgetter

import sys
import os
import datetime
import gzip
import shutil
import string
import time
import fileinput
import io
import argparse

import requests
import json
import base64

import codecs

import zipfile
import hashlib
import random
import numpy as np
import tensorflow as tf

import re
from concurrent.futures import ThreadPoolExecutor
from bs4 import BeautifulSoup
from collections import defaultdict

import traceback

from adpcaptcha import get_captcha, load_captcha_model

# Number of votes required
REQUIRED = 4717396

# Number of top letters to check for a quick update
TOP_LETTERS_CHECK = 4

# folder containing public data (html website)
WEB_ROOT = "./"

# folder containing private data(anticaptcha key, cookies)
KEYS_ROOT = "./../"

DATA_COMMUNES = "data_communes.raw.json"
DATA_COMMUNES_OUT = "data_communes.json"
DATA_DPTS = "data_departements.raw.json"
DATA_DPTS_OUT = "data_departements.json"

NO_SOUTIEN = "Aucun soutien n'est accept"
MAINTENANCE = "Maintenance en cours"


MAX_THREADS = 13

''' UPDATE 17/06 20:00 -> google captcha ajoute (je croyais le gvt anti-GAFAM? mais que se passe-t-il?)
    une fois le captcha valide, il vous ajoute des cookies, les charger ici.

    Les cookies sont au format
        nom du cookie:valeure du cookie

    par exemple (exemple fictif, copiez les bonnes valeure dans COOKIES_FILE):
        incap_ses_947_2147127:NB0dVrcXCTLgsuFPTR1tAZDRC20BBBBB90doQ9EMO3A17bibEFCZhS==
    nlbi_2273138:HIgYKgijXxVPOcHYqtI6oACAABB01KNiVcvDJ7KvzAn7AONK
    visid_incap_2173329:oEQt5bXrUlS57PldhfLbQZHRC10AAAAAQkIPABCDABCAOv+MAVL+vokisqxxwcZWBzOpqHANnUoj
    A10_Insert-12472:ABAEOJBKFABC
    PHPSESSID:bcofoi71acfsa4mph2jvg4d7c7
        etc...

    Si vous utilisez https://addons.mozilla.org/fr/firefox/addon/cookies-txt/ pour firefox, la ligne de commande
    qui permets de transformer les cookies au bon format:
        $ cat ~/Downloads/cookies.txt | grep "interieur.gouv.fr" | sed -e "s/[[:space:]]\+/ /g" | awk '{print $6":"$7}' > ../cookies.dat

    NOTE: cookie? et la RGPD dans tout ca? l'europe devrait mettre une amende a ce site du gouvernement... a moins qu'il soit au dessus de ca.

    NOTE 2: seulement le cookie incap_ses_xxx_xxxxx est maintenant necessaire (laisse ce commentaire par prudence)
'''
COOKIES_FILE = os.path.join(KEYS_ROOT, "cookies.dat")
COOKIES = {}
with open(COOKIES_FILE) as cookie:
    cookies = cookie.readlines()
    for c in cookies:
        data = c.split(":")
        COOKIES[data[0]] = data[1].rstrip("\r\n")


def trim_eol(data):
    '''Enleve tous les sauts de ligne de data '''
    return data.replace("\r","").replace("\n","")


def parse_data(data, insee, I, J, TS):
    '''Parse le contenu de data et retourne une liste de communes en utf8'''
    soup = BeautifulSoup(data, 'html.parser')
    result = []

    for line in soup.find("tbody").find_all('tr'):
        city = line.find_all('td')[2]
        city_name = "fr_etranger"
        if city.contents:
            result.append(str(city.contents[0]))
            city_name = str(city.contents[0])
        else:
            result.append("fr_etranger")

    soup.decompose()
    soup = None

    return result

def get_num_pages(data):
    '''
    Extrait la liste des numéro de page pour un élément IJ donné. La première page est ignorée.

    Exemple d'HTML à parser:

    <div class="pagination">
        <span class="current">1</span>

        <span class="page"> <a href="/consultation_publique/8/M/ME?page=2">2</a> </span>
        ...
        <span class="page"> <a href="/consultation_publique/8/M/ME?page=10">10</a> </span>

        <span class="next"> <a href="/consultation_publique/8/M/ME?page=2">&gt;</a> </span>
        <span class="last"> <a href="/consultation_publique/8/M/ME?page=30">&gt;&gt;</a> </span>
    </div>
    '''
    soup = BeautifulSoup(data, 'html.parser')
    result = 0
    last_page = soup.find("span", class_="last")

    if last_page:
        match = re.search(r'\?page=(\d+)$', last_page.a['href'])
        last_page = int(match.group(1))
        result = last_page

    soup.decompose()
    soup = None

    return result

def create_insee_index(filename):
    '''Récupère la liste des villes depuis DATA_INSEE + créer un index par nom de ville'''
    insee_data = None
    with open(filename, "r") as f:
        insee_data = json.load(f)

    insee_index = defaultdict(lambda: {'code_insee': [], 'count': 0})
    for code_insee, data in insee_data.items():
        insee_index[data['nom']]['code_insee'].append(code_insee)

    return insee_data, insee_index

ARRONDISSEMENTS = [("Paris","75056"),("Lyon","69123"),("Marseille","13055")]
def add_soutien(insee_index, ville):
    '''Ajoute un soutien pour la ville donnée
       Pn estime le nombre de soutiens pour une ville ayant des homonymes en fonction de son nombre d'électeurs et du nombre d'électeurs
       des autres villes homonymes. cf https://gitlab.com/pierrenn/adprip/issues/7

       Pour les arrondissements, "Paris" est la somme des soutiens de tous les arrondissements parisien.
    '''
    insee_index[ville]['count'] += 1

    #gestion des arrondissements
    for arr in ARRONDISSEMENTS:
        if arr[0]+" " in ville: #les arrondissements sont dans le HTML au format du type "Paris 1er xxxx"
            insee_index[arr[0]]['count'] += 1


def create_session():
    session = requests.Session()
    for key, cookie in COOKIES.items():
        if key.startswith("incap_ses_"):
            cookies = {key: cookie}
            requests.utils.add_dict_to_cookiejar(session.cookies, cookies)

    if not requests.utils.dict_from_cookiejar(session.cookies):
        raise Exception("impossible de trouver le cookie 'incap_ses_...' dans les cookies")

    return session


def url_dll(url, session):
    '''Télécharge une url'''
    result = ""
    while len(result) == 0:
        try: #URL bug par moment: OpenSSL.SSL.ZeroReturnError
            rep = session.get(url)
            while len(rep.text) == 0:
                time.sleep(0.5)
                rep = requests.get(url)

            result = rep.text
            if MAINTENANCE in result:
                print("[" + url + "]:\t Maintenance en cours, attente 10s et reessaie", file=sys.stderr)
                result = ""
                time.sleep(10)

        except Exception as e:
            print("Exeception for url '"+url+"': "+str(e), file=sys.stderr)
            time.sleep(1)

    return result

def gen_pij_url(url, idx, I, J):
    '''Given a letter couple IJ and a pagination index idx, return correct URL; if J is '_', skip it'''
    ret = url + "/" + I + "/" + I

    if J != '_':
        ret += J

    if idx != 1:
        ret +=  "?page="+str(idx)

    return ret

def bypass_captcha(url, session, has_captcha_failed = (lambda data: "Captcha" in data and "Saisir les caract" in data), post_dict = {}, form_token_name = "form[_token]", form_captcha_name = "form[captcha]"):
    '''Dll page at url with session en bypassant le captcha qui a le token form_token_name et en envoyant sa valeur dans form_captcha_name'''
    data = trim_eol(url_dll(url, session))
    if "Request unsuccessful. Incapsula incident ID" in data or "https://www.google.com/recaptcha/api2/bframe" in data: #need to generate a new set of cookie
        print(COOKIES, file=sys.stderr)
        print("Incapsula cookie stop, exiting.", file=sys.stderr)
        raise Exception("incapsula cookie error")

    orig_data = data
    while has_captcha_failed(data) or (MAINTENANCE in data) or ("tbody" not in data and NO_SOUTIEN not in data):
        try:
            soup = BeautifulSoup(data, 'html.parser')
            token = str(soup.find("input", attrs={'name':form_token_name, 'type':'hidden'})['value'])
            soup.decompose()
            soup = None

            image = session.get("https://www.referendum.interieur.gouv.fr/bundles/ripconsultation/securimage/securimage_show.php", stream=True)
            CAPTCHA_VAL = get_captcha(image)

            rep = session.post(url, data={**post_dict, **{form_captcha_name:CAPTCHA_VAL,form_token_name:token}})
            data = trim_eol(rep.text)
        except Exception as e:
            print(traceback.format_exc(), file=sys.stderr)
            print("Erreur lors de la validation/recuperation du captcha [url="+url+"]: "+str(e)+", retry apres 1s...", file=sys.stderr)
            if 'NoneType' in str(e):
                # Reset session
                session = create_session()
            data = orig_data
            time.sleep(1)

    return data, session

def full_check_thread(executor, cpath, url, session, I, J, insee, TS):
    p = 1
    try:
        if not is_cache_locked(cpath): #always try to have the cache locked
            lock_cache(cpath)

        num_pages, last_num_soutiens = find_cache_value(cpath, I, J)
        cache_soutiens = 200 * (num_pages - 1) + last_num_soutiens
        if cache_soutiens == 0:
            print("Skip empty {}{}".format(I, J))
            return []

        print("Fetching "+I+J+"...")
        data, session = bypass_captcha(gen_pij_url(url, 1, I, J), session)
        soutiens = []

        if NO_SOUTIEN in data:
            return soutiens

        for v in parse_data(data, insee, I, J, TS):
            soutiens.append(v)

        pages = get_num_pages(data)  # get other pages
        print("[" + I + "," + J + "," + str(p) + "/" + str(pages) + "]\t page found")
        for p in range(2, pages+1):
            if not is_cache_locked(cpath): #always try to have the cache locked
                lock_cache(cpath)

            data, session = bypass_captcha(gen_pij_url(url, p, I, J), session)
            print("[" + I + "," + J + "," + str(p) + "/" + str(pages) + "]\t page found")
            for v in parse_data(data, insee, I, J, TS):
                soutiens.append(v)

        return soutiens
    except Exception as e: #TODO: better handle exceptions/db count when interruption during multiple pages fetching
        if "ncapsula cookie" in str(e): #if incapsula error, stop all threads
            print("Incapsula cookie error, exiting all threads..", file=sys.stderr)
            executor.shutdown(wait=False)
            sys.exit(-1)

        print(e, file=sys.stderr)
        print(traceback.format_exc(), file=sys.stderr)
        print("STATE WHEN STOPPED: ("+str(I)+","+str(J)+","+str(p)+")", file=sys.stderr)
        raise Exception("fail while doing full data check")

def full_check_data(url, insee_index, cpath):
    '''Verifie les MAJ sur URL et retourne une hashmap: localite ~> nombre de votants'''
    global MAX_THREADS

    load_captcha_model()
    jobs = []

    TS =  str(int(time.time()))
    with ThreadPoolExecutor(max_workers=MAX_THREADS) as executor:
        for I in 'LM' + (string.ascii_uppercase.replace('LM','')): # alphabet with LM pushed in front
            for J in string.ascii_uppercase+'_':
                session = create_session()
                kw = {'executor': executor, 'cpath': cpath, 'url': url, 'session': session, 'I': I, 'J': J, 'insee': insee_index, 'TS': TS}
                jobs.append(executor.submit(full_check_thread, **kw))

    try:
        for jIdx, job in enumerate(jobs):
            soutiens = job.result()
            for s in soutiens:
                add_soutien(insee_index, s)

            if jIdx%50==0:
                print("Saved "+str(jIdx)+"/"+str(len(jobs))+" results.")
    except Exception as e:
        print("ERROR while compiling data (jIdx="+str(jIdx)+"): "+str(e), file=sys.stderr)
        print(traceback.format_exc(), file=sys.stderr)
        return

    print("Done saving results.")

def set_carte_state_json(state):
    print("\tSetting carte state to: "+str(state))
    for line in fileinput.input("archive/latest.count.json", inplace=True):
        data = json.loads(line.rstrip())
        data["carte_state"] = state
        data["timestamp"] = str(int(time.time()))
        print(json.dumps(data))

def maj_compteur_json(count, crawl):
    print("\tNouveau compteur JSON: "+str(count))
    for line in fileinput.input("archive/latest.count.json", inplace=True):
        data = json.loads(line.rstrip())
        data["total"] = count
        data["crawl"] = crawl
        data["timestamp"] = str(int(time.time()))
        print(json.dumps(data))

def read_cache_elem(data):
    '''Lit une ligne du cache. Chaque ligne de cache est au format:
            IJ:numero derniere page:nombre elements derniere page

        si J est le caractere '_', seul la premiere lettre est prise en compte
    '''
    elem = data.strip("\n").split(":")
    if len(elem) != 3:
        print(data, file=sys.stderr)
        print(elem, file=sys.stderr)
        raise Exception("find cache value: fichier cache invalide")

    return elem[0], int(elem[1]), int(elem[2])


def find_cache_value(cpath, I, J):
    '''Trouve une valeure dans le fichier cache pour IJ. Chaque ligne du fichier cache est au format:
            IJ:numero derniere page:nombre elements derniere page
    '''
    if not os.path.exists(cpath):
        open(cpath, 'a').close() # ie touch cpath

    with open(cpath) as cache:
        for c in cache:
            lettres, nb_page, nb_soutien = read_cache_elem(c)
            if I+J != lettres:
                continue

            return nb_page,nb_soutien

    return 1,0 #premiere page, 0 valeure a priori sur la premiere page (personne n'a signe)

def get_total_soutiens_with_cache(cpath):
    '''Lit le fichier de cache et retourne le nombre total de soutiens'''
    nb_soutiens = 0
    with open(cpath) as cache:
        for c in cache:
            lettres, num_pages, nb_soutien_last_page = read_cache_elem(c)
            nb_soutiens += 200*(num_pages-1)+nb_soutien_last_page

    return nb_soutiens

def get_total_pages_in_cache(cpath):
    '''Retourne le nombre total de page a analyser'''
    ret = 0
    with open(cpath) as cache:
        for c in cache:
            lettres, num_pages, nb_soutien_last_page = read_cache_elem(c)
            ret += num_pages

    return ret

SHOULD_UPDATE_CRAWL = False
def update_cache_value(cpath, I, J, last_page, nb_soutien, nb_page_analyse):
    '''MAJ la valeure du cache IJ avec les donnees IJ:last_page:nb_soutien; return true si le cache a ete change'''
    global SHOULD_UPDATE_CRAWL
    hasBeenUpdated = False

    hasFound = False
    for line in fileinput.input(cpath, inplace=True):
       elems = line.split(":")
       if elems[0] == I+J:
           hasFound = True
           if int(elems[1]) != last_page or int(elems[2].rstrip('\r\n')) != nb_soutien:
               hasBeenUpdated = True

           print(I+J+":"+str(last_page)+":"+str(nb_soutien))
       else:
           print(line.rstrip('\r\n'))

    if not hasFound:
        hasBeenUpdated = True
        with open(cpath, "a") as cache:
            cache.write(I+J+":"+str(last_page)+":"+str(nb_soutien)+"\n")

    nb_soutiens = get_total_soutiens_with_cache(cpath)
    if SHOULD_UPDATE_CRAWL:
        total_page_an = 0
        for I_c in string.ascii_uppercase:
            for J_c in string.ascii_uppercase+'_':
                total_page_an += nb_page_analyse[I_c+J_c]

        maj_compteur_json(nb_soutiens, 100*total_page_an/get_total_pages_in_cache(cpath))
    else:
        maj_compteur_json(nb_soutiens, 100)

    return hasBeenUpdated

def lock_cache(cpath):
    open(cpath+".lock", 'a').close() # ie touch

def unlock_cache(cpath):
    os.remove(cpath+".lock")

def is_cache_locked(cpath):
    return os.path.exists(cpath+".lock")

NOMBRE_PAGES_AN = {}
def init_nombre_pages_an():
    global NOMBRE_PAGES_AN
    for I in string.ascii_uppercase:
        for J in string.ascii_uppercase+'_':
            NOMBRE_PAGES_AN[I+J] = 0

def check_cache_data(url, session, cpath, I, J):
    '''Verifie le site du RIP pour I/IJ; maj le cache si necessaire et retourne True si le cache a ete change'''
    global NOMBRE_PAGES_AN
    hasBeenUpdated = False

    print("Updating cache "+I+J+"...")
    page, last_num_soutiens = find_cache_value(cpath, I, J)
    data, session = bypass_captcha(gen_pij_url(url, page, I, J), session)

    if NO_SOUTIEN in data:
        return update_cache_value(cpath,I,J,1,0, NOMBRE_PAGES_AN)

    new_page = get_num_pages(data)
    if new_page > 0 and new_page > page: #update page count if more than 1 page and needed
        page = new_page
        print("\tFound new last page "+str(page)+"...")
        data, session = bypass_captcha(gen_pij_url(url, page, I, J), session)

    soup = BeautifulSoup(data, 'html.parser')
    tbody = soup.find("tbody")
    if tbody == None:
        print("WARN: probably a partial HTTP response (?!), trying again...")
        soup.decompose()
        soup = None

        time.sleep(1)
        return check_cache_data(url, session, cpath, I, J)

    new_nb_soutiens = len(tbody.find_all('tr'))
    print("\tupdating "+I+J+" @ "+str(page)+","+str(new_nb_soutiens))

    soup.decompose()
    soup = None

    NOMBRE_PAGES_AN[I+J] = page
    ret = update_cache_value(cpath,I,J,page,new_nb_soutiens, NOMBRE_PAGES_AN)
    return ret

def update_top_n_cache(url, session, cpath, N, startIdx):
    '''Mets a jour les top N entrees du cache (si N = 0, MAJ toutes les entrees)'''

    TOPn = []
    with open(cpath) as cache:
        for c in cache:
            lettre, nb_page, nb_soutien = read_cache_elem(c)
            TOPn.append((lettre,nb_page,nb_soutien))

    TOPn = sorted(TOPn, key=itemgetter(1,2), reverse=True)[startIdx:]
    if N != 0:
        TOPn = TOPn[0:N]

    #if startIdx != 0: #pour tests
    #   TOPn = TOPn[len(TOPn)-5:] #pour tests
    hasBeenUpdated = False
    for t in TOPn:
        if check_cache_data(url, session, cpath, str(t[0][0]), str(t[0][1])):
            hasBeenUpdated = True

        if not is_cache_locked(cpath): #on doit verifier si on doit repartir de 0
            lock_cache(cpath)
            val = update_top_n_cache(url, session, cpath, TOP_LETTERS_CHECK, 0)
            if val:
                return True

    return hasBeenUpdated

def quick_check_data(url, cpath, top_letters_check = TOP_LETTERS_CHECK):
    '''Essaye de reactualiser les tops pages du cache, reactualise le cache en entier si changements detectes.
       Retourne le nombre de soutien ou -1 si un autre process actualise deja le cache.
    '''
    global NOMBRE_PAGES_AN, SHOULD_UPDATE_CRAWL

    if is_cache_locked(cpath): #si un autre process est en train de MAJ le cache
        unlock_cache(cpath) #signaler au process qui fait la MAJ qu'il faut verifier s'il doit repartir de 0
        return -1

    lock_cache(cpath)
    session = create_session()
    load_captcha_model()
    if update_top_n_cache(url, session, cpath, top_letters_check, 0): #si le site du RIP est mis a jour
        SHOULD_UPDATE_CRAWL = True
        set_carte_state_json(False)
        update_top_n_cache(url, session, cpath, 0, top_letters_check) #mettre le cache a jour
    unlock_cache(cpath)

    return get_total_soutiens_with_cache(cpath)

def write_history(count, cache_path):
    #write history for graph
    TS = str(int(time.time()))
    with open(os.path.join(WEB_ROOT, "archive/history.dat"), "a") as history:
        history.write(TS+";"+str(count)+"\n")

    #write cache for timestamping latest new count
    cache_gzip_path = os.path.join(WEB_ROOT, "archive/compteur", TS + ".cache.dat.gz")
    with open(cache_path, 'rb') as f_in:
        with gzip.open(cache_gzip_path, "w") as f_out:
            shutil.copyfileobj(f_in, f_out)

    return TS

def create_dpts_data(filename):
    dpts_data = None
    with open(filename, "r") as f:
        dpts_data = json.load(f)
        for _, dpt in dpts_data.items():
            dpt['soutiens'] = 0

    return dpts_data

def update_soutiens_data(insee_data, insee_index, dpts_data):
    '''Genere le fichier json des communes pour la carte'''

    for insee,data in insee_data.items():
        nom = data['nom']
        nb_elec = data['electeurs']
        h_insee = data['homonymes']
        h_nb_elec = data['h_electeurs']
        nb_soutiens = insee_index[nom]['count']

        data['soutiens'] = nb_soutiens
        data['h_soutiens'] = 0

        nb_soutiens_final = nb_soutiens
        if h_nb_elec != 0:
            if nb_elec == 0:
                data['electeurs'] = -1
                data['soutiens'] = 0
                continue

            nb_soutiens_final = nb_soutiens*nb_elec//h_nb_elec #mieux d'avoir des erreurs d'arrondis que des données à virgules
            data['soutiens'] = nb_soutiens_final
            data['h_soutiens'] = nb_soutiens

        try:
            if insee[0] == '9' and insee[1] >= '7' and insee[1] <= '9': #DOM-TOMs / dpts a 3 chiffres
                dpts_data[insee[0:3]]['soutiens'] += nb_soutiens_final
            elif all(insee != arr[1] for arr in ARRONDISSEMENTS): #ne pas ajouter 2 fois le nombre de soutiens pour les arrondissements
                dpts_data[insee[0:2]]['soutiens'] += nb_soutiens_final
            #et donc les francais de l'etranger sont dans le departement '00'

        except :
            print(insee, file=sys.stderr)
            print(data, file=sys.stderr)
            print(dpts_data, file=sys.stderr)
            sys.exit(1)

def write_json(data, filename):
    with open(filename, "w", encoding='utf-8') as f:
        json.dump(data, f, separators=(',', ':'))

def update_geo_data(insee_data, insee_index, dpts_data):
    '''Dump les donnees de la BDD db_path au format json pour la carte'''
    update_soutiens_data(insee_data, insee_index, dpts_data)

    #archive data
    TS = str(int(time.time()))
    write_json(insee_data, "archive/carte/communes/" + TS + "." + DATA_COMMUNES_OUT)
    write_json(dpts_data, "archive/carte/departements/" + TS + "." + DATA_DPTS_OUT)
    print("TS fichier JSON: "+TS)

def check_recepisse(session, recepisse):
    '''Return:
        - -1 si le numero de recepisse est inconnu dans la DB du ministere
        -  0 si le numero de recepisse est connu dans le DB mais invalide
        -  1 si le numero de recepisse est valide
    '''
    data, session = bypass_captcha("https://www.referendum.interieur.gouv.fr/consultation_recepisse", session, lambda data: not("Accept" in data or "Aucune information relative" in data or " n'a pas été validé" in data), {"recepisse_search_form[recepisse]": recepisse},  "recepisse_search_form[_token]", "recepisse_search_form[captcha][captcha_code]")
    if "Aucune information relative" in data:
        return -1

    if " n'a pas été validé" in data:
        return 0

    if "Accept" in data:
        return 1

    raise Exception("could not detect state of recepisse (data="+data+")")

def is_valid_recepisse(elem):
    '''Verifie que elem est un numero de recepisse valide'''
    if not all(c.isalnum() or c=='-' for c in elem):
        return False

    tirets_pos = [n for n in range(len(elem)) if elem.find('-', n) == n]
    return (len(tirets_pos) == 3 and tirets_pos[0]==5 and tirets_pos[1]==11 and tirets_pos[2]==17)

def only_one(elems, predicate):
    '''Return idx such as predicate(elems[idx])==True iff a single element of elems has predicate (-1 otherwise)'''
    ret = -1
    for lIdx, e in enumerate(elems):
        if predicate(e):
            if ret>=0: #already one valid
                return -1
            else: #not one valid yet
                ret = lIdx

    return lIdx

def check_recepisses(inp_file, separator):
    '''Verifie que les recepisse dans input_file sont valide et pour chaque ligne du fichier d'entrée, rajoute \"'separator'N\" ou N est la sortie de check_recepisse'''
    session = requests.Session()
    load_captcha_model()
    for lIdx, line in enumerate(inp_file.readlines()):
        elems = line.rstrip("\r\n").split(separator)
        elem_idx = only_one(elems, is_valid_recepisse)
        if elem_idx < 0:
            raise Exception("fichier '"+input_file+":"+str(lIdx+1)+"' trouvé aucun ou plusieurs recepisses sur une seule ligne")

        is_valid = check_recepisse(session, elems[elem_idx].upper())
        print(line.rstrip("\r\n") + separator + str(is_valid))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Script pour scrapper le site du RIP de l'intérieur")
    parser.add_argument("action", nargs='?', default="check", help="action à exécuter; 'full' scrappe ttes les villes, 'cache' MAJ tout le cache, 'check' (defaut) vérifie l'absence de nvlle données, 'recepisse' vérifie une liste de récépissé")
    parser.add_argument("input", nargs='?', default=sys.stdin, type=argparse.FileType('r'), help="[valide pour 'recepisse'] entrée contenant une liste de récépissés à vérifier")
    parser.add_argument("-s", "--separator", default=";", help="[valide pour 'recepisse'] separateur à utiliser pour entrées/sorties fichiers de récépissé; default: ';'")
    args = parser.parse_args()

    cache_path = os.path.join(WEB_ROOT, "archive/cache.dat")

    if args.action == "recepisse":
        try:
            check_recepisses(args.input, args.separator)
        except Exception as e:
            print("Erreur de traitement:"+str(e)+", arret...", file=sys.stderr)
            sys.exit(-1)

        sys.exit()

    url = "https://www.referendum.interieur.gouv.fr/consultation_publique/8"
    if args.action == "full":
        insee_data, insee_index = create_insee_index(DATA_COMMUNES)
        dpts_data = create_dpts_data(DATA_DPTS)
        try:
            full_check_data(url, insee_index, cache_path)
            update_geo_data(insee_data, insee_index, dpts_data)
            set_carte_state_json(True)
        except Exception as e:
            print("STD ERROR while crawling data: "+str(e), file=sys.stderr)
            print(traceback.format_exc(), file=sys.stderr)
        except SystemExit as x:
            print("EXIT ERROR while crawling data: "+str(x), file=sys.stderr)
            print(traceback.format_exc(), file=sys.stderr)

        sys.exit()


    init_nombre_pages_an()
    start_soutiens = get_total_soutiens_with_cache(cache_path)
    if args.action == "cache": #maj entiere du cache (pour tests/init du cache)
        SHOULD_UPDATE_CRAWL = True
        count = quick_check_data(url, cache_path, 0)
        if count != 0:
            now_soutiens = get_total_soutiens_with_cache(cache_path)
            maj_compteur_json(now_soutiens, 100)
            if start_soutiens != now_soutiens:
                write_history(now_soutiens, cache_path)
    elif args.action == "check":
        SHOULD_UPDATE_CRAWL = False
        count = quick_check_data(url, cache_path)
        if count > 0:
            now_soutiens = get_total_soutiens_with_cache(cache_path)
            maj_compteur_json(now_soutiens, 100)
            if start_soutiens != now_soutiens:
                write_history(now_soutiens, cache_path)
    else:
        print("Action '"+args.action+"' non reconnue, arrêt...", file=sys.stderr)
        sys.exit(-1)
