var url = new URL(window.location.href)

const isIos = /ip(hone)/i.test(navigator.userAgent)
const isIpad = /ip(ad)/i.test(navigator.userAgent)
const isAndroid = /android/i.test(navigator.userAgent)
const isMobile = isIos || isAndroid || url.searchParams.has('m')

const testTablet = /Tablet|iPad/i.test(navigator.userAgent)
const isTablet = (isMobile && testTablet || isIpad)

// Pour les tests en local, il suffit de charger la page /xxx.html?remote=1
var baseUrl = url.searchParams.has('remote') ? 'https://www.adprip.fr/' : ''
    urlADPRipCarte = baseUrl + 'archive/carte/departements/',
    urlADPRipCarteCommunes = baseUrl + 'archive/carte/communes/',
    suffixADPRipCommunes = '.data_communes.json',
    suffixADPRipDpt = '.data_departements.json'

var frNumberFormat = new Intl.NumberFormat('fr-FR')
function prettyPrintNum(data) {
    return frNumberFormat.format(data)
}

const DPTs = Array(20).fill()
    .map((_,i) => i.toString(10).padStart(2, '0'))  // 00 -> 19
    .concat(["2A", "2B"])
    .concat(Array(75).fill().map((_,i) => (i+21).toString(10)))  // 21 -> 95
    .concat(["971", "972", "973", "974", "975", "976", "977", "978", "986", "987", "988"])
const NB_DEPT = DPTs.length


function formatDepartements(nom) {
    return nom
        .replace(/d([A-Z])/g, "d'$1")
        .replace(/([a-z0-9])([A-Z])/g, '$1 $2')
        .replace('St ', 'Saint ')
        .replace('Miquelon', 'et Miquelon')
        .replace('Territoirede', 'Territoire de')
        .replace(' Et ', ' et ')
        .replace(' Caledonie', '-Calédonie')
        .replace('Fr Etranger', "Français de l'étranger")
}

//Check that feature corresponds to a departement from which we have geoJSON commune detailed data
function hasGeoJSONCommuneData(feature) {
    return true //tous les departements ont leur communes mtnt
}

function getVillePosition(feature) {
    var lnglat = turf.centroid(feature).geometry.coordinates
    return [lnglat[1],lnglat[0]]
}

function generateShareLinks(code, nom, latlng) {
    var villeUrl = url.protocol + '//' + url.hostname + url.pathname + '?insee=' + code
        + '&lat=' + parseFloat(latlng.lat || latlng[0]).toFixed(4) + '&lng=' + parseFloat(latlng.lng || latlng[1]).toFixed(4) + '&zoom=11'
    var twitterTxt = 'Les électeurs de ' + nom + ' veulent le référendum sur la privatisation du groupe ADP.%0A%0AVoir ' + nom + ' sur @adpripfr :%0A'
    return '<div class="share-links"><a target="_blank" title="Lien direct vers '
            + nom + '" href="' + villeUrl + '"><i class="fa fa-link"></i></a>&nbsp;&nbsp;'
            + '<a target="_blank" rel="nofollow noopener noreferrer" title="Partagez les infos de '
            + nom + ' sur twitter" href="http://twitter.com/share?text='
            + twitterTxt + '&url=' + encodeURIComponent(villeUrl) + '&hashtags=RéférendumADP,Palmar&egrave;sADP"><i class="fa fa-twitter"></i></a>'
            + '<a target="_blank" rel="nofollow noopener noreferrer" title="Partagez les infos de '
            + nom + ' sur facebook" href="https://www.facebook.com/sharer/sharer.php?u='
            + encodeURIComponent(villeUrl) + '" style="margin-left: 5px"><i class="fa fa-facebook"></i></a>'
            + '</div>'
}

var MAP_POSITIONS =
[
    {0:  [-7.294922,40.195659,14.128418,52.802761]},     //metropole
    {1:  [-62.083740,15.749963,-60.853271,16.704602]},   //guadeloupe
    {2:  [-61.443787,14.272369,-60.567627,15.027033]},   //martinique
    {3:  [-55.393066,1.609285,-50.679932,6.233395]},     //guyanne
    {4:  [54.453735,-22.095820,56.755371,-20.164255]},   //reunion
    {5:  [-57.354126,46.259645,-55.052490,47.772560]},   //pierre miquelon
    {6:  [44.574280,-13.520508,45.986023,-12.297068]},   //mayotte
    {7:  [-62.924881,17.840218,-62.718544,17.983631]},   //st barth
    {8:  [-63.205032,17.981346,-62.917671,18.207502]},   //st martin
    {9:  [178.846436,-15.718239,186.113892,-11.178402]}, //wallis futuna
    {10: [-155,-28,-134,-7.5]},                          //polynesie
    {11: [160.697021,-24.547123,171.441650,-17.098792]}  //nvlle caledonie
]

function estimateMapPosition(lat, lng) {
    for(var i = 0; i < MAP_POSITIONS.length; i++) {
        var elem = MAP_POSITIONS[i][i]
        if(elem[0] <= lng && lng <= elem[2] && elem[1] <= lat && lat <= elem[3]) {
            return i
        }
    }

    console.log(lng, lat, " position non reconnue")
    return 0
}

function convertlink(text) {
    var urlRegex = /(https?:\/\/[^\s]+)/g;
    return text.replace(urlRegex, function(url) {
        return '<a href="' + url + '" rel="nofollow noopener noreferrer" target="_blank">' + geturl(url) + '</a>';
    })

}

function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? 0 : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

function geturl(url){
    if(url.length > 20){
        return url.substr(0,20) + "...";
    } else {
        return url;
    }
}

function getDepartementFromInsee(insee) {
    if(insee.substring(0,2) == "2A" || insee.substring(0,2) == "2B") {
        return insee.substring(0,2)
    }

    var departement = insee.substring(0,2)
    if(parseInt(departement) <= 95) {
        return departement
    }

    return parseInt(insee.substring(0,3))
}

//geojson cache
var communesGeoJson = {}
function loadCommuneGeoJson(code, callback) {
    //cache hit
    if(code in communesGeoJson) {
        callback(communesGeoJson[code])
        return
    }

    //cache miss
    $.getJSON(baseUrl + 'carto/departements/' + code + '/communes.geojson', function(data){
        communesGeoJson[code] = data;
        callback(data);
    })
}

var timestamps = []
function parseDirListTS(dirList) {
    var elems = dirList.split(suffixADPRipDpt)
    var ret = []
    for(var i = elems.length-2; i > 0; i -= 2) {
        var TS = elems[i].substring(2)
        ret.push(TS)
        timestamps.push(TS)
    }

    return ret
}

function fillDataDateSelect(dirList, dataDate) {
    elemsTS = parseDirListTS(dirList)
    for(var i = 0; i < elemsTS.length; i++) {
        var TS = elemsTS[i]
        var date = moment.unix(TS)
        dataDate.append(
            $('<option>', { value: TS , text: date.utc().format("DD/MM/YYYY") })
        )
    }
    timestamps.sort()
    dataDate.prop("selectedIndex", 0);
    dataDate.data("prev", 0);
    return elemsTS[0] //return most recent TS
}

//data caches
var communesData = {}
var departementsData = {}
function reloadDataTS(newTS, callback) {
    //cache hit
    if(newTS in communesData && newTS in departementsData) {
        callback(departementsData[newTS], communesData[newTS])
        return
    }

    //cache miss
    $.when(
        $.ajax({
            type: "GET",
            url: urlADPRipCarteCommunes + newTS + suffixADPRipCommunes,
            contentType: "application/json; charset=utf-8",
            dataType: "text",
            success: function(data) {
                escapedData = data.replace(/\\u[\da-f]{4}/gi,
                                        function (match) {
                                            return '&#x'+match.replace(/\\u/g, '')+';'
                                        })
                communesData[newTS] = JSON.parse(escapedData);
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log("WARN: error while getting communes", newTS, xht, textStatus, errorThrown)
            }
        }),
        $.getJSON(urlADPRipCarte + newTS + suffixADPRipDpt, function(data) {
            departementsData[newTS] = data
        })
    ).then(function() {
        callback(departementsData[newTS], communesData[newTS], newTS)
    })
}

function getTimestamps(from=0, to=0) {
    return timestamps.slice(from, to || timestamps.length)
}

function loadAllDepartementsTS(callback, from=0, to=0) {
    const toFetch = getTimestamps(from, to)
    var tasks = []
    for (newTS of toFetch) {
        //cache hit
        if (newTS in departementsData) {
            continue
        }
        //cache miss
        (function(newTS) {
            tasks.push($.getJSON(urlADPRipCarte + newTS + suffixADPRipDpt, function(data) {
                departementsData[newTS] = data
            }))
        })(newTS)
    }
    $.when.apply(0, tasks).then(callback)
}

function loadAllCommunesTS(callback, from=0, to=0) {
	const toFetch = getTimestamps(from, to)
	var tasks = []
	for (newTS of toFetch) {
		//cache hit
		if (newTS in communesData && Object.keys(communesData[newTS]).length > 0) {
			continue
		}
		//cache miss
		(function(newTS) {
			tasks.push($.getJSON(urlADPRipCarteCommunes + newTS + suffixADPRipCommunes, function(data) {
				communesData[newTS] = data
			}))
		})(newTS)
	}
	$.when.apply(0, tasks).then(callback)
}

//log timestamp parsing for auto log viewer
function parseLogListTS(dirList) {
    var elems = dirList.split(".log");

    var ret = []
    for(var i = elems.length-2; i > 0; i -= 2) {
        var TS = elems[i].substring(2)
        var size = $.trim(elems[i+1].split('<td align="right">')[2].split('</td>')[0]);
        var sizeB = parseFloat(size)
        var lastChar = size.charAt(size.length-1)
        if(lastChar=='K') {
            sizeB *= 1024
        } else if(lastChar=='M') {
            sizeB *= 1024*1024
        } else if(lastChar<'0' || lastChar>'9') { //G or greater
            sizeB *= 1024*1024*1024
        }

        ret.push([TS,sizeB])
    }

    return ret
}

function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}
