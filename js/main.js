var Num = new Intl.NumberFormat('fr-FR')
var Per = new Intl.NumberFormat('fr-FR', { maximumFractionDigits: 2, minimumFractionDigits: 2})
const avgDays = 7;

var displayLogTS = "";
$.get("archive/logs/", function(data) { //get a list of available data timestamp., extract the last one greater than 1.4K
	var logTS = parseLogListTS(data);
	for(var i = 0; i<logTS.length; i++) {
		if(logTS[i][1]>1500) {
			displayLogTS = logTS[i][0];
			break;
		}
	}
})

var lastDaysProgress = 0, interieurRetention = 0, cc_percent = 1., cc_missing = 1., cc_link = "";
var compteurClone;
function computeEstimParameters(historyData, compteur) {
	compteurClone = compteur

	//0. parse history
	var history = historyData.split(/\r?\n/);
	for(var hIdx = 0; hIdx < history.length; hIdx++) {
		history[hIdx] = history[hIdx].split(';');
		history[hIdx][0] = new Date(history[hIdx][0]*1000);
	}

	//1. get average num of soutiens added for last avgDays days
	var tmp = 0;
	for(var idx = history.length-2-avgDays; idx < history.length-1; idx++) {
		tmp += (history[idx][1]-history[idx-1][1])/history[idx][1];
	}
	lastDaysProgress = tmp / avgDays;

	$.getJSON("./chiffres_cc.json", function(data) {
		var last_elem = data[data.length-1];
		cc_percent = last_elem["cc_percent"];
		cc_link = last_elem["url"];

		$("#cc_link").attr("href", cc_link);

		var numCCpoints = 0;
		var missingSoutiensRatio = 0;
		for(dIdx in data) { //for each CC number
			if(data[dIdx]["interieur_version"] != last_elem["interieur_version"]) { //skip if different rip site listing version
				continue;
			}

			//2. find matching date in history
			var dataDate = Date.parse(data[dIdx]["date"]);
			var historyCCIdx_orig = 0;
			var historyCCIdx_over = 0;
			for(var hIdx = 1; hIdx < history.length; hIdx++) {
				if(history[hIdx][0].getTime() >= dataDate && history[hIdx-1][0] <= dataDate) {
					historyCCIdx_orig = hIdx;
				}
			}

			if(historyCCIdx_orig == 0) {
				console.log("ESTIMATION ERREUR: n'arrive pas a trouver la date dans l'historique correspondant au chiffre CC ", data[dIdx]);
				continue;
			}

			//3. find first number in history where the number of soutien > CC estimation
			historyCCIdx_orig -= 1;
			historyCCIdx_over = historyCCIdx_orig++;
			while(history[historyCCIdx_over][1] < data[dIdx]["comptage_off"] && ++historyCCIdx_over < history.length);
			if(historyCCIdx_over >= history.length-1) { //CC estimation not yet in history
				continue;
			}

			historyCCIdx_over -= 1;
			numCCpoints += 1;
			interieurRetention += historyCCIdx_over - historyCCIdx_orig + 1;
			missingSoutiensRatio += 1 - history[historyCCIdx_over][1] / data[dIdx]["comptage_off"];

		}
		interieurRetention /= numCCpoints;
		cc_missing = 1 + missingSoutiensRatio / numCCpoints;


		get_compteur_data(); //start polling counter
	})
}

function get_compteur_data() {
	$.ajax({ url: "archive/latest.count.json?nocache", success: function(jcount) {
		var counter = Num.format(jcount["total"]),
			required = Num.format(jcount["required"]),
			percent = jcount["total"] / jcount["required"] * 100,
			percentFormatted = Per.format(percent),
			percentEstimation = cc_missing*(1+interieurRetention*lastDaysProgress),
			counterUp = Num.format(Math.round(jcount["total"]*percentEstimation)),
			counterUpEnregistre = Math.round((jcount["total"]*percentEstimation/(cc_percent*100)))*100,
			rejetesEstim = Math.round(counterUpEnregistre*(1-cc_percent)/100)*100,
			counterUpEnregistreFormatted = Num.format(counterUpEnregistre),
			percentFormattedUp = Per.format(percent*percentEstimation);

		$("#main_compteur").replaceWith(compteurClone);

		animateValue($("#counter"), jcount["total"], 2500, 1, Num, "", "");
		animateValue($("#counter_percent"), percent, 2500, 0.01, Per, "(", "%)");
		animateValue($("#max_soutiens"), counterUpEnregistre, 2500, 50, Num, "", "");

		$("#required").text(" / "+required);
		$("#max_soutiens_formatted").text(counterUpEnregistreFormatted);
		$("#estimation").text(counter + " et " + counterUp + " soutiens validés (entre " + percentFormatted +"% et " + percentFormattedUp + "%)");
		$("#percent_est_multiplier").text(Per.format((percentEstimation-1)*100));

		$('[data-toggle="total_tooltip"]').tooltip({
			html: true,
			placement: "bottom",
			title: '<ul class="tip"><li>Estimation &agrave; la hausse faite &agrave; partir des <a rel="nofollow noopener noreferrer" target="_blank" href="'+cc_link+'">chiffres du CC</a> : <li> - entre ' + counter + ' et ' + counterUp + ' soutiens validés (entre ' + percentFormatted +'% et ' + percentFormattedUp + '%)</b></li><li> - '+Num.format(counterUpEnregistre)+' soutiens envoy&eacute;s en tout dont au max. ' + Num.format(rejetesEstim) + ' soutiens ('+ Per.format(100*(1-cc_percent)) +'%) probablement déjà <a rel="nofollow noopener noreferrer" target="_blank" href="https://twitter.com/adpripfr/status/1145952765438394368">rejet&eacute;s</a></li></ul>'
		});

		var logLink = "";
		if(displayLogTS.length > 0) {
			logLink = '<b><i><a target="_blank" style="color:white" href="https://www.adprip.fr/log/?noreverse&ts='+displayLogTS+'">détails</a></b>';
		}

		var crawl = jcount["crawl"];
		if (typeof crawl === 'undefined') {
			crawl = 100;
		}

		$('#progress-bar').css('width', crawl + '%');
		$('#progress-bar').css('aria-valuenow', crawl);

		//bar strip animation
		if(crawl < 100 || !jcount["carte_state"]) {
			$('#progress-bar').addClass("progress-bar-striped progress-bar-animated");
		} else {
			$('#progress-bar').removeClass("progress-bar-striped progress-bar-animated");
		}

		//bar text and color
		var startDiv = '<div style="margin-top:5px">';
		if(crawl >= 100) {
			$('#progress-bar').addClass('bg-success');
			if(jcount["carte_state"]) {
				var timestamp = new Date(jcount["timestamp"]*1000);
				$('#progress-bar').html(startDiv+'Compteur et carte à jour (dernière vérification: '+timestamp.toLocaleString('fr-FR', { timeZone: 'Europe/Paris' })+' - '+logLink+')</div>');
			} else {
				$('#progress-bar').html(startDiv+'Compteur à jour, carte en cours de MAJ ('+logLink+')...</div>');
			}
		} else {
			$('#progress-bar').removeClass('bg-success');
			$('#progress-bar').html(startDiv+'Liste du ministère modifiée... ('+Per.format(crawl)+'% des pages analysées - '+logLink+')</div>');
		}
	}, dataType: "json", timeout: 3000, complete: () => { setTimeout(function() { get_compteur_data(); }, 5000); } });
}

function animateValue(obj, end, duration, increment, formatter, prepend, postpend) {
	if(obj.text().length == 0) {
		obj.text(prepend+formatter.format(end)+postpend);
		obj.data("value", end);
		return;
	}

	var start = obj.data("value")
	if(end == start) {
		return;
	}

	var range = end - start;
	var numStep = range / increment;
	var stepTime = Math.abs(Math.floor(duration / numStep));
	while(stepTime < 10) {
		increment *= 10;
		numStep = range / increment;
		stepTime = Math.abs(Math.floor(duration / numStep));
	}

	obj.data("value", end);
	var current = start;
	var timer = setInterval(function() {
		current += increment;
		if(current >= end) {
			current = end;
		}

		obj.text(prepend+formatter.format(current)+postpend);
		if (current == end) {
			clearInterval(timer);
		}
	}, stepTime);
}

function generateCards(dataDepartements, cb) {
	var cards = [];
	for(var d in dataDepartements) {
		if(d != "93" && d != "974" && dataDepartements[d]["soutiens"]/dataDepartements[d]["electeurs"] > 0.01) {
			cards.push(["count",formatDepartements(dataDepartements[d]["nom"]),Num.format(dataDepartements[d]["soutiens"]), d]);
			cards.push(["percent",formatDepartements(dataDepartements[d]["nom"]),Per.format(100*dataDepartements[d]["soutiens"]/dataDepartements[d]["electeurs"])+' %', d]);
		}
	}

	shuffleArray(cards);
	var sections = [["days"]];
	$.getJSON("index_card.json", function(iCard) {
		for(var cIdx = 0; cIdx < cards.length; cIdx++) {
			sections.push(cards[cIdx], ["days"]);
			if(Math.random()>0.75) {
				var elem = iCard[Math.floor(Math.random() * Math.floor(iCard.length))]
				sections.push(["sentence", elem["title"], elem["text"]])
			}
		}

		cb(generateSlides(sections))
	})
}


function generateSlides(sections) {
	var num_days = Num.format(Math.round(Math.max(0, new Date(2020, 2, 12, 23) - new Date())/(1000*60*60*24)));

	var slider = "";
	for(var sIdx = 0; sIdx < sections.length; sIdx++) {
		var slide = sections[sIdx];

		// fix pour IOS 12+ où l'on se retrouve avec le site bloqué et une page blanche ?
		// data-transistion les valeurs concave et convex ne sont pas supportées apparemment ... à confirmer
		slider += (!isTablet) ? '<section data-transition="concave">' : '<section data-transition="zoom">'; // zoom ou default pour tablet
		slider += '<p class="card_text" style="line-height:inherit;font-style:italic;margin-bottom:80px;font-size:2.1em;">Sauvons ADP !</p>';

		if(slide[0] == "count" || slide[0] == "percent") {
			// TODO : ajouter un blason 00.png pour "fr-etranger" / handle that case
			slider += '<img src="images/blasons/'+slide[3]+'.png" style="margin: 0;border: none;background: none;box-shadow: none;line-height: 1.1em;margin-bottom: 15px;" />';
			slider += '<p class="card_text" style="font-size:1.58em;line-height: 1.1em;margin-bottom: 30px;">'+slide[1]+'</p>';
			slider += '<p class="card_text" style="font-size:3.33em;line-height: 0.8em;margin-bottom: 20px;">'+slide[2]+'</p>';
			if(slide[0] == "count") {
				slider += '<p class="card_text" style="font-size:1.41em;line-height: 1.1em;margin-bottom: 80px;">Signatures</p>';
			} else { //percent
				slider += '<p class="card_text" style="font-size:1.41em;line-height: 1.1em;margin-bottom: 80px;">Signataires dans le département</p>';
			}
		} else if(slide[0] == "days") {
			//C'est fini !
			//slider += '<p class="card_text" style="font-size:2em;line-height: 1.1em;margin-bottom: 30px;text-transform:uppercase;margin: 230px 0;">Dernier jour<br>pour signer !</p>';
		} else if(slide[0] == "sentence") {
			slider += '<p class="card_text" style="font-size:2em;line-height: 1.1em;margin-bottom: 0;text-transform:uppercase;margin-top: 212px;">'+slide[1]+'</p>';
			slider += '<p class="card_text" style="font-family: \'Arial\', sans-serif;font-size:0.83em;margin:0 auto;line-height: 1.1em;margin-bottom: 212px;max-width: 85%;">'
				+slide[2]+'</p>';
		} else {
			console.log("WARNING: slide non reconnue", slide)
		}
		/*
		slider += '<p class="card_text" style="font-size: 1.253em;margin:0;line-height: 1em;font-weight: 450;">Pour signer, Rendez-vous sur :<br>';
		slider += '<a rel="nofollow noopener noreferrer" target="_blank" href="https://www.referendum.interieur.gouv.fr/soutien/etape-1">referendum.interieur.gouv.fr</a></p><br>';
		slider += '<p class="card_text" style="font-size: 1.253em;;margin:0;line-height: 1em;font-weight: 450;">Plus d\'infos <a href="signez.html" target="_blank">ici</a></p>';
		*/

		//buttons not working ?
		//slide += '<p class="m1-txt1 p-b-36" style="color:lightgrey"><a class="btn btn-danger p-l-30 p-r-30" rel="nofollow noopener noreferrer" target="_blank" href="https://www.referendum.interieur.gouv.fr/soutien/etape-1"><span style="font-size: 22px"><i class="fa fa-pencil"></i>&nbsp;&nbsp;Signez sur</span><br/>referendum.interieur.gouv.fr</a></p><p class="m1-txt1 p-b-36" style="color:lightgrey"><a class="btn btn-info p-l-30 p-r-30" rel="nofollow noopener noreferrer" target="_blank" href="https://www.adprip.fr/signez.html"><span style="font-size: 22px"><i class="fa fa-info"></i>&nbsp;&nbsp;Signer ADP ?</span><br/>Un référendum ? Pourquoi ?</a></p>'

		slider += '</section>';
	}

	return slider;
}

function appendCssSheet(isMobile) {
	if(!isMobile) {
		$('head').append('<link rel="stylesheet" type="text/css" href="node_modules/reveal.js/css/reveal.css" />');
		$('head').append('<link rel="stylesheet" type="text/css" href="node_modules/reveal.js/css/theme/black.css" />');
	}

	$('head').append('<link rel="stylesheet" type="text/css" href="css/util.css" />');
	$('head').append('<link rel="stylesheet" type="text/css" href="css/main.css?nocache" />');
}
