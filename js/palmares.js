$(document).ready(function() {
    var villes = null
    var departements = null
    var currentDataTs = null
    var chefs_lieux = new Set()
    var palmaresDepartementsTable
    var palmaresCommunesTable
    var page = parseInt(url.searchParams.get('p'), 10) || 0
    var pageLength = parseInt(url.searchParams.get('pl'), 10) || 10
    var dataForTableDepartement = null
    var plotLoaded = false
    var plotComLoaded = false
    var plotFrom = parseInt(url.searchParams.get('f'), 10) || 0
    var plotTo = parseInt(url.searchParams.get('t'), 10) || 0
    var fullView = url.searchParams.has('full')

    // Force last 15 days on mobile to reduce data
    if (!fullView) {
        if (plotFrom == 0) plotFrom = -15
    } else {
        $('#chartSwitch').html('Vue simplifi&eacute;e').attr("href", "palmares.html?vue=dhist")
    }

    $('#header').load(baseUrl + 'navigation.html')
    $('#stats_disclaimer').load(baseUrl + 'stats_disclaimer.html')

    function formatArrondissements(nom) {
        return nom.replace(/([a-zA-Z]+)([0-9]+)(er?)Arrondissement/, '$1&nbsp;$2$3&nbsp;Arrondissement')
            .replace(/-/g, '&#8209;')
    }
    function shouldHideDpt(deptCode) {
                //TAAFs                Clipperton          Autres
        return deptCode == "984" || deptCode == "989" || deptCode == "999"
    }

    // Replace browser url to ease page sharing
    function refreshURL() {
        if (!window.history.replaceState) {
            return
        }
        var params = new URLSearchParams(),
            param_dpt_q = $('#PalmaresDepartements_filter input').val(),
            param_com_q = $('#PalmaresCommunes_filter input').val(),
            param_dpt = $('#departementFilter').val(),
            param_arr = $('#arrondissementFilter').val(),
            param_min = $('#minElecteurs').val(),
            param_max = $('#maxElecteurs').val(),
            pageInfo

        if ($('#pills-communes-tab').hasClass('active')) {
            pageInfo = palmaresCommunesTable.page.info()
            params.set('vue', 'com')
            if (param_com_q) params.set('q', param_com_q)
            if (param_dpt) params.set('dpt', param_dpt)
            if (param_arr === '0') params.set('arr', param_arr)
            if (param_min) params.set('min', param_min)
            if (param_max) params.set('max', param_max)
            $('#footer').show()
        } else if ($('#pills-dpt-tab').hasClass('active')) {
            pageInfo = palmaresDepartementsTable.page.info()
            params.set('vue', 'dept')
            if (param_dpt_q) params.set('q', param_dpt_q)
            $('#footer').show()
        } else if ($('#pills-dpthist-tab').hasClass('active')) {
            pageInfo = {page: 0, length: 10}
            params.set('vue', 'dhist')
            if (plotFrom !== 0) params.set('f', plotFrom)
            if (plotTo !== 0) params.set('t', plotTo)
            if (page !== 0) params.set('p', page)
            if (isMobile) params.set('m', '')
            if (fullView) params.set('full', '')
            $('#footer').hide()
        } else {
            param_min = $('#chist-minElecteurs').val()
            param_max = $('#chist-maxElecteurs').val()
            param_top = $('#chist-topFilter').val()
            param_insee = $('#chist-code').val()
            pageInfo = {page: 0, length: 10}
            params.set('vue', 'chist')
            if (plotFrom !== 0) params.set('f', plotFrom)
            if (plotTo !== 0) params.set('t', plotTo)
            if (param_min) params.set('min', param_min)
            if (param_max) params.set('max', param_max)
            if (param_top) params.set('top', param_top)
            if (param_insee) params.set('insee', param_insee)
            $('#footer').hide()
        }
        if (pageInfo.page > 0) params.set('p', pageInfo.page)
        if (pageInfo.length != 10) params.set('pl', pageInfo.length)

        if (url.protocol === 'https:') {
            window.history.replaceState({}, null, window.location.pathname + '?' + params)
        }
    }

    // Palmares des departements
    function tableDepartements() {
        var dptFilter = $('#departementFilter')

        // Re-init departement dropdown
        dptFilter.empty()
        dptFilter.append('<option value="">Tous les d&eacute;partements</option>')
        dptFilter.append('<option value="¹">Chefs-lieux de d&eacute;partements</option>')
        dptFilter.append('<option disabled="disabled">───────────────────────────</option>')

        $.each(DPTs, function(i, dpt) {
            departements[dpt].nom = formatDepartements(departements[dpt].nom)
            dptFilter.append(
                $('<option>', { value: dpt, text: dpt + ' - ' + departements[dpt].nom})
            )
            departements[dpt].nom = departements[dpt].nom.replace(/-/g, '&#8209;') //utiliser tirets insecables pour le tableau
        })

        // Select departement from url
        var urlDepartement = url.searchParams.get('dpt')
        if ($('#departementFilter option[value="' + urlDepartement + '"]').length > 0) {
            $('#departementFilter').val(urlDepartement)
        }

        dataForTableDepartement = $.map(departements, function(deptObj, deptCode) {
            var pourcent = 0
            if (deptObj.soutiens > 0 && deptObj.soutiens < deptObj.electeurs) {
                pourcent = 100.0 * deptObj.soutiens / deptObj.electeurs
            }
            return [[
                deptCode,
                deptObj.nom,
                pourcent,
                deptObj.soutiens,
                deptObj.electeurs
            ]]
        })
        .sort(function(d1, d2) {
            return d2[2] - d1[2]
        })
        .map(function(item, index) {
            item.unshift(index + 1)
            item[3] = item[3].toFixed(2)
            return item
        })

        if (palmaresDepartementsTable) {
            palmaresDepartementsTable.clear()
            palmaresDepartementsTable.rows.add(dataForTableDepartement)
            palmaresDepartementsTable.draw()
        } else {
            var buttonCommon = {
                exportOptions: {
                    format: {
                        body: function (data, row, column, node) {
                            var out = data.replace( /&nbsp;/g, '' )
                                .replace(/&#8209;/g, '-')
                                .replace(/%/, '')
                                .replace(/,/, '.')
                            if (column === 0) {
                                return out.replace(/e/, '')
                            } else if (column === 1) {
                                return out.replace(/^0/, '')
                            }
                            return out
                        }
                    }
                }
            }
            palmaresDepartementsTable = $('#PalmaresDepartements').DataTable({
                data: dataForTableDepartement,
                language: {
                    url: baseUrl + 'js/datatable_french.json'
                },
                deferRender: true,
                searchDelay: 500,
                search: {
                    search: url.searchParams.get('q') || ''
                },
                displayStart: page * pageLength,
                pageLength: pageLength,
                buttons: [
                    $.extend(true, {}, buttonCommon, {
                        extend: 'excelHtml5',
                        className: 'btn-sm',
                        autoFilter: true,
                    }),
                    $.extend(true, {}, buttonCommon, {
                        extend: 'csvHtml5',
                        className: 'btn-sm'
                    })
                ],
                columns: [
                    {
                        title: 'Rang',
                        className: 'text-right small font-weight-bold',
                        render: $.fn.dataTable.render.number('&nbsp;', ',', 0, '', '&nbsp;e')
                    },
                    {
                        title: 'Code',
                        className: 'text-right'
                    },
                    {
                        title: 'D&eacute;partement'
                    },
                    {
                        title: 'Pourcentage',
                        render: $.fn.dataTable.render.number('&nbsp;', ',', 2, '', '&nbsp;%'),
                        className: 'text-right',
                    },
                    {
                        title: 'Soutiens',
                        render: $.fn.dataTable.render.number('&nbsp;', ',', 0),
                        className: 'text-right'
                    },
                    {
                        title: "Nombre d'&eacute;lecteurs",
                        render: $.fn.dataTable.render.number('&nbsp;', ',', 0),
                        className: 'text-right',
                    },
                ],
                order: [
                    [
                        3,
                        'desc',
                    ],
                ],
                initComplete: function() {
                    $('#PalmaresDepartements_wrapper .col-md-6:eq(0)').removeClass('col-md-6').addClass("col-md-4")
                    palmaresDepartementsTable.buttons().container()
                        .appendTo( '#PalmaresDepartements_wrapper .row:eq(0)' )
                        .removeClass('btn-group').addClass('btn-group-sm pull-right')
                }
            })
            palmaresDepartementsTable.on('draw', refreshURL)
            $('#loadingBox').hide()
        }
    }

    //~ // Palmares des villes
    function tableVilles() {
        var dataForTable = $.map(villes, (function(ville, codeInsee) {
            var pourcent = 0
            if (ville.electeurs > 0 && ville.soutiens < ville.electeurs) {
                pourcent = 100.0 * ville.soutiens / ville.electeurs
            }
            var departement = codeInsee.substr(0, 2)
            if (departement === '97' || departement === '98') {
                departement = codeInsee.substr(0, 3)
            }
            var restants = -Math.ceil(0.1 * ville.electeurs - ville.soutiens)
            var chef_lieu = chefs_lieux.has(codeInsee) ? '¹' : ''

            return [[
                departement + chef_lieu,
                codeInsee,
                formatArrondissements(ville.nom),
                pourcent,
                ville.soutiens,
                ville.electeurs,
                restants,
                ville.homonymes.length,
            ]]
        }))
        .sort(function(c1, c2) {
            // Si pourcentage identique, tri par nombre d'electeurs croissant
            if (c2[3] == c1[3]) {
                // Les villes avec -1 and 0 vont en queue de classement
                c1 = c1[5] < 1 ? Number.MAX_SAFE_INTEGER + c1[5] : c1[5]
                c2 = c2[5] < 1 ? Number.MAX_SAFE_INTEGER + c2[5] : c2[5]
                return c1 - c2
            }
            return c2[3] - c1[3]
        })
        .map(function(item, index) {
            item.unshift(index + 1)
            item[4] = item[4].toFixed(2)
            return item
        })

        if (palmaresCommunesTable) {
            palmaresCommunesTable.clear()
            palmaresCommunesTable.rows.add(dataForTable)
            palmaresCommunesTable.draw()
            $('#loadingBox').hide()
        } else {
            var buttonCommon = {
                exportOptions: {
                    format: {
                        body: function (data, row, column, node) {
                            var out = data.replace( /&nbsp;/g, '' )
                                .replace(/&#8209;/g, '-')
                                .replace(/%/, '')
                                .replace(/,/, '.')
                            if (column === 0) {
                                return out.replace(/e/, '')
                            } else if (column === 1 || column === 2) {
                                return out.replace(/^0/, '')
                            } else if (column === 3) {
                                return out.replace(/<.*/g,'')
                                    .replace(/&#x(\w+);/g, function(match, dec) {
                                        return String.fromCharCode(parseInt(dec, 16))
                                    })
                            }
                            return out
                        }
                    }
                }
            }
            palmaresCommunesTable = $('#PalmaresCommunes').DataTable({
                data: dataForTable,
                language: {
                    url: baseUrl + 'js/datatable_french.json'
                },
                deferRender: true,
                searchDelay: 500,
                search: {
                    search: url.searchParams.get('q') || ''
                },
                displayStart: page * pageLength,
                pageLength: pageLength,
                buttons: [
                    $.extend(true, {}, buttonCommon, {
                        extend: 'excelHtml5',
                        className: 'btn-sm',
                        autoFilter: true,
                    }),
                    $.extend(true, {}, buttonCommon, {
                        extend: 'csvHtml5',
                        className: 'btn-sm'
                    })
                ],
                columns: [
                    {
                        title: 'Rang',
                        className: 'text-right small font-weight-bold',
                        render: $.fn.dataTable.render.number('&nbsp;', ',', 0, '', '&nbsp;e')
                    },
                    {
                        title: 'D&eacute;pt.',
                        className: 'text-right',
                        render: function (data, type, row) {
                            return type === 'display' ? data.replace('¹', '') : data
                        }
                    },
                    {
                        title: 'Insee',
                        className: 'text-right',
                        visible: false
                    },
                    {
                        title: 'Commune',
                        render: function (data, type, row) {
                            return data + (row[8] == 0 ? '' :
                                '<span style="cursor: pointer" title="Voir note en bas de page">*</span>')
                        }
                    },
                    {
                        title: 'Pourcentage',
                        render: $.fn.dataTable.render.number('&nbsp;', ',', 2, '', '&nbsp;%'),
                        className: 'text-right',
                    },
                    {
                        title: 'Soutiens', render: $.fn.dataTable.render.number('&nbsp;', ',', 0),
                        className: 'text-right'
                    },
                    {
                        title: "Nombre d'&eacute;lecteurs",
                        render: $.fn.dataTable.render.number('&nbsp;', ',', 0),
                        className: 'text-right',
                    },
                    {
                        title: "Diff soutiens pour 10%",
                        render: function (data, type, row) {
                            if (type !== 'display') {
                                return row[7]; // for sorting
                            }
                            if (row[7] >= 0) {
                                const val = $.fn.dataTable.render.number('&nbsp;', ',', 0).display(row[7]);
                                return '<span style="color:green" title="'+val+' soutiens au dessus de 10 %">+ '+val+'</span>';
                            } else {
                                const val = $.fn.dataTable.render.number('&nbsp;', ',', 0).display(-row[7]);
                                return '<span style="color:red" title="'+val+' soutiens manquants pour atteindre 10 %">'+val+'</span>'
                            }
                        },
                        className: 'text-right',
                    }
                ],
                order: [
                    [
                        4,
                        'desc',
                    ],
                ],
                initComplete: function(settings, json) {
                    $('#PalmaresCommunes_wrapper .col-md-6:eq(0)').removeClass('col-md-6').addClass("col-md-4")
                    palmaresCommunesTable.buttons().container()
                        .appendTo( '#PalmaresCommunes_wrapper .row:eq(0)' )
                        .addClass('btn-group-sm pull-right')
                        .removeClass('btn-group')
                    toggleExport()

                    $('#loadingBox').hide()
                    if (url.hash === '#communes' || 'com' === url.searchParams.get('vue')) {
                        $('#pills-communes-tab').trigger('click')
                    }
                },
            })
            palmaresCommunesTable.on('draw', refreshURL)
        }
    }

    const colors = ['#ff2222', '#fb5f5f', '#fd5d89', '#f267b1', '#d878d4',
         '#b28aee', '#8a9bfd', '#59a9ff', '#00b5ff', '#00c8d4', '#52cb7c']

    function chartDepartements() {
        var trace = {
            x: [], y: {}
        }
        var toShow = dataForTableDepartement.map(function(d) {return d[1]})
        const timestampSlice = getTimestamps(plotFrom, plotTo)

        $.each(toShow, function(i, dpt) {
            trace.y[dpt] = {
                name: departements[dpt].nom,
                y:[],
                soutiens: [],
                rank: i
            }
        })

        for (timestamp of timestampSlice) {
            var deptObj = departementsData[timestamp]
            var date = moment.unix(timestamp)
            trace.x.push(date.utc().format('YYYY-MM-DD'))

            $.each(toShow, function(i, dpt) {
                var percent = 100.0 * deptObj[dpt].soutiens / deptObj[dpt].electeurs
                trace.y[dpt].y.push(percent)
                trace.y[dpt].soutiens.push(prettyPrintNum(deptObj[dpt].soutiens))
            })
        }

        var data = []
        $.each(trace.y, function(item) {
            // TODO Creer un menu qui montre les meilleur progressions en couleur
            //var lastIndex = trace.y[item].y.length - 1
            //var last = trace.y[item].y[lastIndex]
            //var first = trace.y[item].y[lastIndex - 15]
            //var progression = (last - first) / first
            var legendgroupindex = Math.floor(trace.y[item].rank / 10)

            if (!fullView) {
                legendgroup = ''
                name = item + ' - ' + trace.y[item].name
                showlegend = true
                color = ''
                visible = (legendgroupindex == page)
            } else {
                legendgroup = 'Rang ' + (10*legendgroupindex+1) + ' à ' + (10*legendgroupindex+10)
                name = legendgroup
                showlegend = (trace.y[item].rank % 10 == 0)
                color = colors[legendgroupindex]
                visible = true
            }

            data.push({
                x: trace.x,
                y: trace.y[item].y,
                name: name,
                text: trace.y[item].soutiens,
                showlegend: showlegend,
                legendgroup: legendgroup,
                legendgroupindex: legendgroupindex,
                type: 'scatter',
                //progression: progression,
                color: color,
                visible: visible,
                hovertemplate: '<b>' + trace.y[item].name + ' (' + item
                    + ')</b><br>Soutiens : %{text}<br>Taux : %{y:.2f}%<extra></extra>'
            })
        })
        data.sort(function(d1, d2) {
            if (d1.legendgroupindex == d2.legendgroupindex) {
                return d2.y[d2.y.length - 1] - d1.y[d1.y.length - 1]
            }
            return d1.legendgroupindex - d2.legendgroupindex
        })

        // Buttons to filter
        function ranks(min, max) {
            return Array(NB_DEPT).fill().map((_,i) => i >= min && i < max)
        }
        buttons = !fullView ? [] : [{ label: 'Tous les départements', method: 'restyle', args: ['visible', ranks(0, NB_DEPT+10)] }]
        for (var i=1; i<=NB_DEPT; i+=10) {
            buttons.push({ label: 'Rang '+i+' à '+(i+9), method: 'restyle', args: ['visible', ranks(i-1, i+9)] })
        }
        var layout = {
            hovermode: 'closest',
            colorway: data.map(function(item) {return item.color}),
            xaxis: {
                showspikes: true,
                spikethickness: 1,
                spikedash: 'dot'
            },
            yaxis: {
                ticksuffix: '&nbsp;%',
            },
            legend: {
                orientation: !fullView ? 'h' : 'v'
            },
            updatemenus: [{
                x: 0.05,
                xanchor: 'left',
                y: 1.07,
                yanchor: 'top',
                buttons: buttons,
                active: page
            }],
            margin: {
                l: 50,
                r: 5,
                b: 50,
                t: 50,
                pad: 4
            }
        }
        var config = {locale: 'fr', responsive: true, staticPlot: isIos || isAndroid || isTablet}

        Plotly.newPlot('chartDepartements', data, layout, config)
        .then(gd => {
            gd.on('plotly_hover', d => {
                var curveNumber = d.points[0].curveNumber
                var vals = gd.data.map((_, i) => i === curveNumber ? 1 : 0.2)
                Plotly.restyle(gd, 'opacity', vals)
            })
            gd.on('plotly_unhover', d => {
                Plotly.restyle(gd, 'opacity', gd.data.map((_, i) => 1))
            })
            gd.on('plotly_restyle', d => {
                if (gd.layout.updatemenus.length > 0) {
                    page = gd.layout.updatemenus[0].active
                    refreshURL()
                }
            })
            $('#chartExportButton').click(function() {
                Plotly.downloadImage(gd, {
                    filename: 'adprip-departements',
                    format: 'png',
                    width: gd._fullLayout.width,
                    height: gd._fullLayout.height
                })
            })
            $('#loadingBox').hide()
        })
    }

    function dataChartCommunes() {
        var trace = {
            x: [], y: {}
        }
        var toShow = []
        const timestampSlice = getTimestamps(plotFrom, plotTo)

        var searchCode = $('#chist-code').val() || null
        var minElecteurs = $('#chist-minElecteurs').val() || 1
        var maxElecteurs = $('#chist-maxElecteurs').val() || 10000000
        var top = $('#chist-topFilter').val() || 1000
        var cleanSearchCode = removeAccents(searchCode)
        var isNaNSearchCode = isNaN(searchCode)

        $.each(communesData[timestampSlice[timestampSlice.length-1]], function(insee, ville) {
            if ((searchCode == null || insee.startsWith(searchCode)
                    || (isNaNSearchCode && removeAccents(ville.nom).includes(cleanSearchCode)))
                    && ville.electeurs >= minElecteurs
                    && ville.electeurs <= maxElecteurs) {
                toShow.push(insee)
                trace.y[insee] = {
                    name: ville.nom,
                    y:[],
                    soutiens: []
                }
            }
        })

        for (timestamp of timestampSlice) {
            var villeObj = communesData[timestamp]
            var date = moment.unix(timestamp)
            trace.x.push(date.utc().format('YYYY-MM-DD'))

            $.each(toShow, function(i, insee) {
                if (villeObj.hasOwnProperty(insee)) {
                    var percent = 100.0 * villeObj[insee].soutiens / villeObj[insee].electeurs
                    trace.y[insee].y.push(percent)
                    trace.y[insee].soutiens.push(prettyPrintNum(villeObj[insee].soutiens))
                } else {
                    console.log(insee + ' not found in ts ' + timestamp)
                }
            })
        }

        var data = []
        $.each(trace.y, function(item) {
            legendgroup = ''
            name = item + ' - ' + trace.y[item].name
            showlegend = true

            data.push({
                x: trace.x,
                y: trace.y[item].y,
                name: name,
                text: trace.y[item].soutiens,
                showlegend: showlegend,
                type: 'scatter',
                //progression: progression,
                hovertemplate: '<b>' + trace.y[item].name + ' (' + item
                    + ')</b><br>Soutiens : %{text}<br>Taux : %{y:.2f}%<extra></extra>'
            })
        })
        data.sort(function(d1, d2) {
            return d2.y[d2.y.length - 1] - d1.y[d1.y.length - 1]
        })
        // Keep only top X
        data = data.slice(0, top)
        return data
    }

    function getLayout(data) {
        return {
            hovermode: 'closest',
            colorway: data.map(function(item) {return item.color}),
            xaxis: {
                showspikes: true,
                spikethickness: 1,
                spikedash: 'dot'
            },
            yaxis: {
                ticksuffix: '&nbsp;%',
            },
            legend: {
                orientation: isMobile ? 'h' : 'v'
            },
            margin: {
                l: 50,
                r: 5,
                b: 50,
                t: 50,
                pad: 4
            }
        }
    }
    var chistConfig = {locale: 'fr', responsive: true, staticPlot: isIos || isAndroid || isTablet}

    function chartCommunes() {
        var data = dataChartCommunes()

        Plotly.newPlot('chartCommunes', data, getLayout(data), chistConfig)
        .then(gd => {
            gd.on('plotly_hover', d => {
                var curveNumber = d.points[0].curveNumber
                var vals = gd.data.map((_, i) => i === curveNumber ? 1 : 0.2)
                Plotly.restyle(gd, 'opacity', vals)
            })
            gd.on('plotly_unhover', d => {
                Plotly.restyle(gd, 'opacity', gd.data.map((_, i) => 1))
            })
            gd.on('plotly_afterplot', d => {
                refreshURL()
            })
            $('#chartExportComButton').click(function() {
                Plotly.downloadImage(gd, {
                    filename: 'adprip-communes',
                    format: 'png',
                    width: gd._fullLayout.width,
                    height: gd._fullLayout.height
                })
            })
            $('#loadingBox').hide()
        })

        $('#chist-minElecteurs, #chist-maxElecteurs, #chist-code, #chist-topFilter').keyup(
            _.debounce(function() {
                var data_update = dataChartCommunes()
                Plotly.react('chartCommunes', data_update, getLayout(data_update), chistConfig)
            }, 300)
        )
        $('#chist-topFilter').change(
            _.debounce(function() {
                var data_update = dataChartCommunes()
                Plotly.react('chartCommunes', data_update, getLayout(data_update), chistConfig)
            }, 300)
        )
    }

    function reload(newDate) {
        // Load departement/commune data using cache from utils.js
        reloadDataTS(newDate, function(data_departements, data_communes) {
            $.each(data_departements, function(deptCode) {
                if (shouldHideDpt(deptCode)) {
                    delete data_departements[deptCode]
                }
            })
            departements = data_departements
            villes = data_communes

            tableDepartements()
            tableVilles()
            if ('dhist' === url.searchParams.get('vue')) {
                $('#pills-dpthist-tab').trigger('click')
            } else if ('chist' === url.searchParams.get('vue')) {
                $('#pills-comhist-tab').trigger('click')
            }
        })
    }

    // Get a list of available data timestamps
    $.when(
        $.get(urlADPRipCarte , function(data) {
            currentDataTs = fillDataDateSelect(data, $('select#dataDate'))
        }),
        $.getJSON('carto/chefs-lieux.json', function(data) {
            chefs_lieux = new Set(data)
        })
    ).then(function() {
        reload(currentDataTs)
    })

    $('select#dataDate').on('change', function () {
        if (this.value != $(this).data("prev")) {
            $('#loadingBox').show()
            reload(this.value)
        }
    })

    function removeAccents(data) {
        return !data
        ? ''
        : typeof data === 'string'
          ? data
                .replace(/\n/g, ' ')
                .replace(/[áâàä]/g, 'a')
                .replace(/[éêèë]/g, 'e')
                .replace(/[íîï]/g, 'i')
                .replace(/[óôö]/g, 'o')
                .replace(/[úüù]/g, 'u')
                .replace(/[ÿ]/g, 'y')
                .replace(/ñ/g, 'n')
                .replace(/æ/g, 'ae')
                .replace(/œ/g, 'oe')
                .replace(/ç/g, 'c')
                .replace(/&#8209;/g, '-')
          : data
    }

    //~ // Search sans accents
    $.fn.dataTableExt.ofnSearch['string'] = removeAccents

    // Filtres
    $.fn.dataTable.ext.search.push(function(settings, data, dataIndex) {
        // Pas de filtre pour la vue par departement
        if (settings.sTableId === 'PalmaresDepartements') {
            return true
        }
        var dptFilter = $('#departementFilter').val()
        var arrFilter = $('#arrondissementFilter').val()
        var min = parseInt($('#minElecteurs').val(), 10)
        var max = parseInt($('#maxElecteurs').val(), 10)
        var dataDpt = data[1]
        var dataVille = data[3]
        var dataElecteurs = parseInt(data[6]) || 0

        if (
            (isNaN(min) || dataElecteurs >= min) &&
            (isNaN(max) || dataElecteurs <= max) &&
            (!dptFilter || dataDpt.startsWith(dptFilter) || dataDpt.endsWith(dptFilter)) &&
            (arrFilter === '1' || !dataVille.includes('Arrondissement'))
        ) {
            return true
        }
        return false
    })

    // Tab change events
    $('#pills-communes-tab, #pills-dpt-tab, #pills-dpthist-tab, #pills-comhist-tab').on('shown.bs.tab', refreshURL)
    $('#pills-dpthist-tab').on('shown.bs.tab', function() {
        if (plotLoaded) return
        plotLoaded = true
        $('#loadingBox').show()
        loadAllDepartementsTS(chartDepartements, plotFrom, plotTo)
    })
    $('#pills-comhist-tab').on('shown.bs.tab', function() {
        if (plotComLoaded) return
        plotComLoaded = true
        $('#loadingBox').show()
        loadAllCommunesTS(chartCommunes, plotFrom, plotTo)
    })

    // Populate filters
    $('#minElecteurs').val(parseInt(url.searchParams.get('min'), 10) || '1')
    $('#maxElecteurs').val(parseInt(url.searchParams.get('max'), 10) || '')
    $('#arrondissementFilter').val(url.searchParams.get('arr') === '0' ? 0 : 1)

    // Populate communeHistory filters
    $('#chist-minElecteurs').val(parseInt(url.searchParams.get('min'), 10) || '')
    $('#chist-maxElecteurs').val(parseInt(url.searchParams.get('max'), 10) || '')
    $('#chist-topFilter').val(parseInt(url.searchParams.get('top'), 10) || 10)
    $('#chist-code').val(url.searchParams.get('insee'))

    // Show/hide export buttons to not propose export of full commune data (too slow)
    function toggleExport() {
        $('#PalmaresCommunes_wrapper .dt-buttons').toggle($('#departementFilter').val() !== '')
    }

    // Event listener to the two range filtering inputs to redraw on input
    $('#minElecteurs, #maxElecteurs').keyup(
        _.debounce(function() {
            palmaresCommunesTable.draw()
        }, 300)
    )
    $('#departementFilter, #arrondissementFilter').change(
        _.debounce(
            function() {
                toggleExport()
                palmaresCommunesTable.draw()
            },
            300,
            { leading: true }
        )
    )
    $('button.toggle-vis').on('click', function (e) {
        e.preventDefault()
        // Get the column API object
        var column = palmaresCommunesTable.column($(this).attr('data-column'))
        // Change button text
        if (column.visible()) {
            $(this).text($(this).text().replace('Masquer', 'Afficher'))
        } else {
            $(this).text($(this).text().replace('Afficher', 'Masquer'))
        }
        // Toggle the visibility
        column.visible(!column.visible())
    })
})
