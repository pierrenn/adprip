Les contributions sont les bienvenues.
Le code est sous GPLv3 donc si vous contribuez, vous acceptez le fait que votre code soit sous licence GPLv3.

N'hesitez pas a faire une MR sur https://gitlab.com/pierrenn/adprip/ ou venir tchatter avec nous sur t.me/adpripfr pour avoir des idees de contribution/savoir dans quelle direction partir.

Pour telecharger une version complete du site, avec toutes les donnees, vous pouvez faire:

```
$ mkdir fancy_name && cd fancy_name
$ git clone git@gitlab:YourID/adprip.git
$ cd adprip/
$ wget -r -np -nH -R "index.html*" https://www.adprip.fr/archive/
$ wget -r -np -nH -R "index.html*" https://www.adprip.fr/images/blasons/
```

Pour mettre a jour les donnees dans votre environnement local:

```
$ #avec firefox, installer https://addons.mozilla.org/en-US/firefox/addon/export-cookies-txt/, aller sur https://www.referendum.interieur.gouv.fr/consultation_publique/8 , valider le reCaptcha, exporter les cookies dans "~/Downloads/cookies.txt"
$ cat ~/Downloads/cookies.txt | grep "interieur.gouv.fr" | sed -e "s/[[:space:]]\+/ /g" | awk '{print $6":"$7}' > ../cookies.dat
$ pip3.7 install -r requirements.txt #utiliser python 3.7 pour comptabilité, 3.5 passe mais certains conflits..
$ python3.7 ./update #verifie que pas de nouveaux soutiens, mets le compteur a jour si nouveaux soutiens
$ python3.7 ./update full  #retelecharge une nouvelle carte
#####
$ # le cookie s'invalide toutes les ~20mns, donc vous pouvez lancer un cron sur votre machine pour empecher le cookie de s'invalider, e.g sur le serveur on a:
$ crontab -l | grep update
7-59/15 * * * * ( cd ~/fancy_name/adprip && nice python3.7 ./update )
$ #si le site du RIP se mets a jour, le cookie s'invalide, il faut donc repeter l'etape avec Firefox
```

Enfin, le site est statique donc vous pouvez le servir avec ce que vous voulez tant que vous executez en amont:
```
$ npm install
```

